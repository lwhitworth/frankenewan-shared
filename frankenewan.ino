#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Streaming.h>

// How many seconds of deep sleep we want between checks if we receive the sleep command (1800 = 30 minutes)
#define SLEEP_DELAY_IN_SECONDS  1800

// Trigger words to enable states
const char* triggerString = "elephant";
const char* sleepString = "giraffe";

// Strings for some of the outputs to MQTT and serial
const char* outputStringTrigger = "Trigger received, Ewan is now listening";
const char* outputStringSleep = "Sleep word received, Ewan will sleep for 30 minutes";
const char* outputStringActive = "Sound detected, Ewan sound activated";
const char* outputStringFinish = "Wait time over. Ewan is listening again";

// WiFi configuration
const char* ssid     = "ssid";
const char* password = "wifipassword";

// MQTT configuration
const char* mqtt_server = "192.168.1.8";
const char* mqtt_username = "frankenewan";
const char* mqtt_password = "fwef234923u4ondsfsdfsdlkfnwknfwe34reu23";
const char* mqtt_control_topic = "home/elliesbedroom/frankenewan/control";
const char* mqtt_status_topic = "home/elliesbedroom/frankenewan/log";
const char* mqtt_status_topic_detections = "home/elliesbedroom/frankenewan/debug";
const char* mqtt_state_topic = "home/elliesbedroom/frankenewan/state";
const char* mqtt_soundstate_topic = "home/elliesbedroom/frankenewan/sound/state";
const char* mqtt_soundcount_topic = "home/elliesbedroom/frankenewan/sound/count";

// Pin that relay is connected to
const int relayPin =  D5;

// Number of seconds to wait when noise is detected (1200 = 20 minutes)
const long interval = 1200;  

// How many readings we use in our smoothing array
const int numReadings = 5;

// Initiate the array for smoothing array
int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

// Declare various global strings and ints that we use
String payloadtextword = "";
String payloadtextnumber = "";
String payloadtextdebug = "";
int payloadtextnumberint = 0;
int payloadtextdebugint = 0;
int triggerCount = 0;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  // Setup serial port
  Serial.begin(115200);
  
  // Setup WiFi
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  // Setup the relay pin and ensure it's turned off
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin,HIGH);

  // Setup the array for the audio sensor readings
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
}

// Setup the WiFi
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  } 
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Setup what occurs when we receive a message on the MQTT channel
void callback(char* topic, byte* payload, unsigned int length) {
  String payloadtext = "";
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.println("] ");
  for (int i = 0; i < length; i++) {
    payloadtext += (char)payload[i];
  }
  Serial.println("Payload text is: " + payloadtext);

  // For loop which will separate the String in parts
  // and assign them the the variables we declare
  for (int i = 0; i < payloadtext.length(); i++) {
    if (payloadtext.substring(i, i+1) == ",") {
      payloadtextword = payloadtext.substring(0, i);
      payloadtextnumber = payloadtext.substring(i+1, i+3);
      payloadtextnumberint = payloadtextnumber.toInt();
      payloadtextdebug = payloadtext.substring(i+4);
      payloadtextdebugint = payloadtextdebug.toInt();
      break;
    }
  }
  // For debugging to see the various parts of the receieved message have been split correctly
  Serial.println("Control word: " + payloadtextword);
  Serial.println("Trigger value: " + payloadtextnumber);
  Serial.println("Debug flag: " + payloadtextdebug);
}

// MQTT connection/reconnection code
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client-FEwan", mqtt_username, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial << "failed, rc=" << client.state() << " try again in 5 seconds" << endl;
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
  // Once connected subscribe to our control topic to get messages
  client.subscribe(mqtt_control_topic);
}

void loop() {
  // First connect MQTT
  if (!client.connected()) {
    reconnect();
  }
  // Once connected we should receive control messages to instruct what to do next.  At present this will either be trigger or sleep.  If neither have been received
  // we'll just keep checking again until we get something useful
  client.loop();
  String payloadtextStatusMessage = payloadtextword + "," + payloadtextnumber + "," + payloadtextdebug;
  // Actions for when the trigger word is receieved
  if (payloadtextword == triggerString) {
    // Record to both the serial connection and to MQTT status and state channels that we've received the trigger word
    Serial << "We received the trigger word \"" << triggerString << "\", trigger value: " << payloadtextnumber << ", debug value: " << payloadtextdebug << endl;
    client.publish(mqtt_status_topic, outputStringTrigger);
    String triggerVolumeSupplied = "Trigger value supplied is: " + payloadtextnumber + ", debug value supplied is: " + payloadtextdebug;
    client.publish(mqtt_status_topic, triggerVolumeSupplied.c_str());
    client.publish(mqtt_state_topic, payloadtextStatusMessage.c_str());
    // Briefly fire the relay as feedback that we're now listening (and to ensure batteries haven't completely drained)
    digitalWrite(relayPin, LOW);
    delay(3000);
    digitalWrite(relayPin,HIGH);
    delay(3000);
    // Now loop through the following indefinitely until we get a new message via the control channel
    while(payloadtextword == triggerString){
      // Subtract the last reading from the smoothing array:
      total = total - readings[readIndex];
      // Read from the sensor (connected to A0):
      readings[readIndex] = analogRead(A0);
      // Add the reading to the total:
      total = total + readings[readIndex];
      // Advance to the next position in the smoothing array:
      readIndex = readIndex + 1;
      // If we're at the end of the array...
      if (readIndex >= numReadings) {
        // ...wrap around to the beginning:
        readIndex = 0;
      }
      // Calculate the average:
      average = total / numReadings;
      // Send the current average value to the Serial connection
      Serial.println(average);
      delay(1);  // Delay in between reads for stability
      // If debug has been enabled in the MQTT initialisation message then send the value to the specified channel
      if ( payloadtextdebugint == 1) {
        String sensorValueString = String(average);
        client.publish(mqtt_status_topic_detections, sensorValueString.c_str());
      } else {
        delay(1);
      }
      delay(100);
      // If the average is louder than the set limit then do the following
      if (average >= payloadtextnumberint){
        // Ouput to the serial and to MQTT
        Serial << "Sound detected, initiating Ewan" << endl;
        client.publish(mqtt_status_topic, outputStringActive);
        client.publish(mqtt_soundstate_topic, "on", true);
        String triggeredVolume = "Sound value detected was: " + String(average) + ", configured trigger value is " + payloadtextnumber;
        client.publish(mqtt_status_topic, triggeredVolume.c_str());
        triggerCount = triggerCount + 1;
        Serial << "We have triggered " << triggerCount << "times" << endl;
        client.publish(mqtt_soundcount_topic, String(triggerCount).c_str());
        // Fire the relay
        digitalWrite(relayPin, LOW); // turn on
        delay(500);
        digitalWrite(relayPin,HIGH); // turn off
        // Wait for 1 second multiplied by the number set in the interval variable
        for (unsigned long i=1; i <= interval; i++){
          Serial << "I have been waiting for " << i << " seconds out of " << interval << endl;
          delay(1000); // delay 1 second
        }
        // Ensure that we're still connected to the MQTT broker
        if (!client.connected()) {
          reconnect();
        }
        // Log to serial and MQTT that we're ready and listening again
        Serial << "Wait time over, Ewan is listening again" << endl;
        client.publish(mqtt_status_topic, outputStringFinish);
        client.publish(mqtt_soundstate_topic, "off", true);
        // Reset the smoothing array so we don't immediately fire again
        total = 0;
        readIndex = 0;
        for (int thisReading = 0; thisReading < numReadings; thisReading++) {
        readings[thisReading] = 0;
        average = 0;
        }
      }
      // Ensure that we're still connected to the MQTT broker
      if (!client.connected()) {
        reconnect();
      }
      // If we haven't seen an average value higher than our configured trigger value loop back and check again
      client.loop();
    }
  }
  // Actions for when the sleep word is receieved
  else if (payloadtextword == sleepString) {
    // Log to serial and MQTT that we've received the sleep word and are going to deep sleep for the configured time
    Serial << "We received the sleep word \"" << sleepString << "\", sleeping for " << SLEEP_DELAY_IN_SECONDS << " seconds" << endl;
    client.publish(mqtt_status_topic, outputStringSleep);
    client.publish(mqtt_state_topic, payloadtextStatusMessage.c_str());
    triggerCount = 0;
    client.publish(mqtt_soundcount_topic, String(triggerCount).c_str());
    // Close open connections for MQTT and WiFi
    Serial.println("Closing MQTT connection...");
    client.disconnect();
    Serial.println("Closing WiFi connection...");
    WiFi.disconnect();
    // Go into deep sleep
    ESP.deepSleep(SLEEP_DELAY_IN_SECONDS * 1000000, WAKE_RF_DEFAULT);
    delay(500);   // wait for deep sleep to happen
  }
  // Actions for when we either don't yet have a message from the control channel, or the message doesn't match any of our configured values
  else {
    Serial.println("No useful message yet so loop and check again");
    delay(1000);
  }
}

